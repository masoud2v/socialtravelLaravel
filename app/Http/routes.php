<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::auth();

//Route::get('/home', 'HomeController@index');

Route::group([
  'prefix' => 'api/v1',
  'namespace' => 'Api'
], function () {
    Route::post('/auth/register', [
    'as' => 'auth.register',
    'uses' => 'AuthController@register'
  ]);
  Route::post('/auth/login', [
    'as' => 'auth.login',
    'uses' => 'AuthController@login'
  ]);
});

Route::group(['middleware' => 'jwt.auth'], function()
{
    Route::resource('users', 'UserController');
    Route::resource('travel', 'TravelController');
    Route::get('usertravel', 'TravelController@getUserTravel');
    Route::get('userparticipated', 'TravelController@getUserParticipated');
    Route::resource('comments', 'CommentController');
    Route::resource('pictures', 'PictureController');
    Route::resource('videos', 'VideoController');
    Route::get('usercomment', 'CommentController@getUserComments');
    Route::resource('bookmark', 'BookmarkController');
});

Route::get('/travels', [
    'before' => 'jwt-auth',
    function () {
        $token = JWTAuth::getToken();
//        dd($token->value);
        $user = JWTAuth::toUser($token);
        $travels = \App\Travel::all();

        return Response::json([
                'travels' => $travels
        ]);
    }
]);

Route::get('/restricted', [
    'before' => 'jwt-auth',
    function () {
        $token = JWTAuth::getToken();
//        dd($token->value);
        $user = JWTAuth::toUser($token);

        return Response::json([
            'data' => [
                'email' => $user->email,
                'registered_at' => $user->created_at->toDateTimeString()
            ]
        ]);
    }
]);
