<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;

class AuthController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(RegisterRequest $request)
    {
//        dd($request);

        if ($request->hasFile('image')) {
            $name = str_random(10);
            $file = $request->file('image');
            $image_path = $name.'.'.$file->extension();
            $request->file('image')->move('images', $name.'.'.$file->extension());
        }else{
            $image_path = "-";
        }

        $newUser = $this->user->create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'image' => $image_path
        ]);
        if (!$newUser) {
            return 'error';
//            return response()->json(['failed_to_create_new_user'], 500);
        }


        //TODO: implement JWT
        $token = JWTAuth::fromUser($newUser);
        $user = JWTAuth::toUser($token);

        return response()->json(array('user'=>$user, 'token'=>$token));
//        return response()->json(compact('token'));

//        return response()->json(['user_created']);
    }

    public function login(LoginRequest $request)
    {
        //TODO: authenticate JWT
        $credentials = $request->only('email', 'password');

        if ( ! $token = JWTAuth::attempt($credentials)) {
            return response()->json(['result' => "ایمیل یا رمز عبور اشتباه است"], 404);
        }
        
        $user = JWTAuth::toUser($token);

        return response()->json(array('user'=> $user, 'token'=> $token));
    }
}
