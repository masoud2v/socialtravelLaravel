<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\Travel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//use Tymon\JWTAuth\JWTAuth;

class TravelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
//        $travels = Travel::all();

//        $travels = Travel::join('users', 'travels.user_id', '=', 'users.id')
//            ->rightJoin('bookmarks', function ($join) use ($user) {
//                $join->on('bookmarks.user_id', '=', $user->id);
//                $join->on('bookmarks.travel_id', '=', 'travels.id');
//            })
//            ->select('travels.*', 'users.image')->latest('travels.created_at')->paginate(10);

//        $travels = DB::table('travels')
        $travels = Travel::join('users', 'travels.user_id', '=', 'users.id')
//            ->join('users', 'travels.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) use ($user) {
                $join->on('travels.id', '=', 'bookmarks.travel_id')
//                    ->on('travels.id', '=', 'bookmarks.travel_id');
                    ->where('bookmarks.user_id', '=', $user->id);
            })
            ->select('travels.*', 'users.image', DB::raw("CASE WHEN bookmarks.bookmarked = 1 THEN 1 ELSE 0 END AS bookmarked"))
//            ->select('travels.*', 'users.image', 'bookmarks.bookmarked')
            ->latest('travels.created_at')
            ->paginate(10);
        return response()->json($travels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if ($request->get('title') != null
            && $request->get('description') != null
            && $request->get('start') != null
            && $request->get('start_id') != null
            && $request->get('start_name') != null
            && $request->get('finish') != null
            && $request->get('finish_id') != null
            && $request->get('finish_name') != null
        ) {
            $travel = new Travel();
            $travel->user_id = $user->id;
            $travel->title = $request->get('title');
            $travel->description = $request->get('description');
            $travel->start = $request->get('start');
            $travel->start_id = $request->get('start_id');
            $travel->start_name = $request->get('start_name');
            $travel->finish = $request->get('finish');
            $travel->finish_id = $request->get('finish_id');
            $travel->finish_name = $request->get('finish_name');
            $travel->timestamps = Carbon::now();
            $travel->save();
            return response()->json(compact('travel'));
        } else {
            $result = "false response";
            return response()->json(compact('result'));
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $travel = DB::table('travels')->where('travels.id', '=', $id)
            ->join('users', 'travels.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) use ($user) {
                $join->on('travels.id', '=', 'bookmarks.travel_id')
                    ->where('bookmarks.user_id', '=', $user->id);
            })
            ->select('travels.*', 'users.image', DB::raw("CASE WHEN bookmarks.bookmarked = 1 THEN 1 ELSE 0 END AS bookmarked"))
//            ->select('travels.*', 'users.image')
            ->get();
        return response()->json(compact('travel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('travels')->where('id', '=', $id)->delete();
        return response()->json(['result'=> true], 200);
    }

    public function search(Request $request)
    {
        $title = $request->get('title');
        $travels = Travel::where('title', 'like', "%$title%")
            ->latest('created_at')
            ->paginate(10);

        return response()->json($travels);
    }

    public function getUserTravel()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $travels = Travel::join('users', 'travels.user_id', '=', 'users.id')->
        where('travels.user_id', $user->id)->select('travels.*', 'users.image')->latest('travels.created_at')->paginate(10);

        return response()->json($travels);
    }

    public function getUserParticipated(){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $travel = DB::table('comments')->where('comments.user_id', '=', $user->id)
            ->rightJoin('travels', 'travels.id', '=', 'comments.travel_id')
            ->rightJoin('users', 'travels.user_id', '=', 'users.id')
            ->select('travels.*', 'users.image')
            ->distinct()
            ->latest('travels.created_at')
            ->paginate(10);

        return response()->json($travel);
    }
}
