<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{

    protected $casts = [
        'bookmarked' => 'boolean',
    ];

//    public function getBookmarkAttribute($value)
//    {
//        return (string) $value;
//    }
}
