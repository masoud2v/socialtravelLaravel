<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use Carbon\Carbon;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserComments(){
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $comments = Comment::join('users', 'comments.user_id', '=', 'users.id')->
        where('comments.user_id', $user->id)->select('comments.*', 'users.image')->latest('comments.created_at')->paginate(10);

        return response()->json($comments);
    }

    public function index(Request $request)
    {
        $travelId = $request->get('travel');
        $comments = Comment::join('users', 'comments.user_id', '=', 'users.id')->
        where('comments.travel_id', $travelId)->select('comments.*', 'users.image')->latest('comments.created_at')->paginate(10);

        return response()->json($comments);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if ($request->get('title') != null
            && $request->get('travelId') != null
            && $request->get('description') != null
            && $request->get('finish') != null
            && $request->get('finish_id') != null) {
            $comment = new Comment();
            $comment->user_id = $user->id;
            $comment->title = $request->get('title');
            $comment->description = $request->get('description');
            $comment->finish = $request->get('finish');
            $comment->finish_id = $request->get('finish_id');
            $comment->travel_id = $request->get('travelId');
            $comment->timestamps = Carbon::now();
            $comment->save();
            return response()->json(compact('comment'));
        }else{
            $result = "false response";
            return response()->json(['result' => $result], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('comments')->where('id', '=', $id)->delete();
        return response()->json(['result'=> true], 200);
    }
}
