-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2017 at 07:16 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `socialtravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2017_01_08_070145_create_travel_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `travels`
--

CREATE TABLE `travels` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `finish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `travels`
--

INSERT INTO `travels` (`id`, `user_id`, `title`, `description`, `start`, `finish`, `created_at`, `updated_at`) VALUES
(1, '1', 'first', 'desc', '35.6892,51.3890', '51.3890,35.6892', '2017-01-09 07:40:17', '2017-01-09 07:40:17'),
(2, '1', 'sec', 'desc', '35.6897,51.3896', '51.3880,35.6893', '2017-01-11 02:07:13', '2017-01-11 02:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mas', 'mas@y.com', '$2y$10$A5sliUddQ3K93si7jBahZ.FlAs7iEkbwSrnjzRRQmJHtxU7Ol51/.', '', NULL, '2017-01-08 04:26:16', '2017-01-08 04:26:16'),
(2, 'min', 'min@y.com', '$2y$10$ggkwM4tIv8BoTatG9XVz2evCYG587ZXGwFjnwf6Ys7.7sxao2RPbK', '', NULL, '2017-01-08 05:15:56', '2017-01-08 05:15:56'),
(3, 'min', 'min2@y.com', '$2y$10$ux1kRso7TQdTW4zcZAptQOH6cxV5YLpT7NuFNx9lrAxFbpJh/wciO', '', NULL, '2017-01-08 05:17:48', '2017-01-08 05:17:48'),
(4, 'mas', 'mass@y.com', '$2y$10$mr0h2RI2Txz2sOyx0Syine2sFOPSHTAZO3RBTOG698E5ex/cnNPhm', '', NULL, '2017-01-08 05:26:23', '2017-01-08 05:26:23'),
(5, 'mas', 'mas23s@y.com', '$2y$10$.4/4sywMBFszn.At8nKkGOG2Kx/x4Os93YQHzq9Nin9FXg3YMcX.y', '', NULL, '2017-01-08 05:27:33', '2017-01-08 05:27:33'),
(6, 'mas', 'mas2a3s@y.com', '$2y$10$T2LInYYVyDPSBH3WmRCG4umxiCmFworpPXWSjmRJZbMc1Mx34oDzG', '', NULL, '2017-01-09 05:03:34', '2017-01-09 05:03:34'),
(7, 'mm', 'mer@y.com', '$2y$10$0rlTUAPYUz/t149fc2S6juW0A9z8NISXb8wEbh.FpTxhX11IPmcw2', '', NULL, '2017-01-09 05:07:38', '2017-01-09 05:07:38'),
(8, 'ss', 'ss@y.com', '$2y$10$7/DffwWrkJkaAgN7tPbxzuztlAE48WJMlRQgWAmoK0KdmQf2xbWM.', '', NULL, '2017-01-09 05:09:00', '2017-01-09 05:09:00'),
(9, 'gg', 'gg@y.com', '$2y$10$22qWoX/Z43cSzZtE9qkvFeK.a979D2B8zuOjUNdQSWHkZp7SGhYSC', '', NULL, '2017-01-09 05:13:00', '2017-01-09 05:13:00'),
(10, 'yy', 'y@t.com', '$2y$10$.ozegrIfsjHVgWvernGxOeoHxXuGdW1v6OvMvQ0UlP.NaUju2lWXS', '', NULL, '2017-01-09 09:25:39', '2017-01-09 09:25:39'),
(11, 'hh', 'hh@y.com', '$2y$10$emgdSoA8blUOICaOLqxDt.X7ht1Vila0Iyy9psKMOgsO5EKwGrEra', '', NULL, '2017-01-09 09:29:25', '2017-01-09 09:29:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `travels`
--
ALTER TABLE `travels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `travels`
--
ALTER TABLE `travels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
