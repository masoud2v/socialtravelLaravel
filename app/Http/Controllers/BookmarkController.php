<?php

namespace App\Http\Controllers;

use App\Bookmark;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class BookmarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $bookmark = Bookmark::where('bookmarks.user_id', '=', $user->id)
            ->leftJoin('travels', function ($join) use ($user) {
                $join->on('travels.id', '=', 'bookmarks.travel_id')
                    ->where('bookmarks.user_id', '=', $user->id);
            })
            ->leftJoin('users', 'travels.user_id', '=', 'users.id')
            ->distinct()
            ->select('travels.*', 'users.image')
            ->latest('travels.created_at')
            ->paginate(10);
        return response()->json($bookmark);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->get('travel_id') != null) {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            DB::table('bookmarks')->where('travel_id', '=', $request->get('travel_id'))->where('user_id', $user->id)->delete();

            $bookmark = new Bookmark();
            $bookmark->user_id = $user->id;
            $bookmark->travel_id = $request->get('travel_id');
            $bookmark->bookmarked = true;
            $bookmark->save();
            return response()->json(['bookmark' => $bookmark, 'result' => true], 200);
        } else {
            return response()->json(['result' => false], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
//        dd($id);

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        DB::table('bookmarks')->where('travel_id', '=', $id)->where('user_id', $user->id)->delete();
        return response()->json(['result' => true], 200);
    }
}
