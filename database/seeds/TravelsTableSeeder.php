<?php

use Illuminate\Database\Seeder;

class TravelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('travels')->insert([
            'user_id' => 5,
            'title' => str_random(10).'@title',
            'description' => 'desc',
            'start' => '10.10.10',
            'finish' => '90.90.90'
        ]);
    }
}
