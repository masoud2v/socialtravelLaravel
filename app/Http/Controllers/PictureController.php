<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Picture;
use Illuminate\Http\Request;

use App\Http\Requests;

class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $commentId = $request->get('comment');
        $travels = Picture::where('comment_id', $commentId)->
        latest('pictures.created_at')->paginate(10);

        return response()->json($travels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');
        if ($file != null
        && $request->get('comment_id')
        && $request->get('title')
        && $request->get('description')) {
            if ($file->isValid()) {
                $file = $request->file('image');
                $file->move('images', $file->getClientOriginalName());
                $picture = new Picture();
                $picture->comment_id = $request->get('comment_id');
                $picture->title = $request->get('title');
                $picture->description = $request->get('description');
                $picture->url = $request->root().'/images/'. $file->getClientOriginalName();
                $picture->save();
                return response()->json(compact('picture'));
            }
        } else {
            return response()->json(['picture' => 'incorrect request'], 400);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
